pMax = 10;
var defpal = [16773855,16764025,16755230,15531993,13369239,14019327,9948159,9151447,14646839,12082973,13834264,16775598,15785113];
palette = [defpal,defpal,defpal,defpal];

try
{
   checkEmbed();
}

catch(e)
{
}

function decode62(n)
{
   if(n >= 48 && n <= 58)
   {
      return n - 48;
   }
   if(n >= 65 && n <= 90)
   {
      return n - 65 + 10;
   }
   if(n >= 97 && n <= 122)
   {
      return n - 97 + 36;
   }
   return 63;
}

function _init(data, dam, pflag)
{
   _p0._box._visible = false;
   cl = data.split(";");
   if(data == null || cl.length == 1)
   {
      var _loc4_ = 0;
      cl = new Array();
      var _loc3_ = 0;
      while(_loc3_ < data.length)
      {
         var _loc2_ = decode62(data.charCodeAt(_loc3_));
         cl.push(_loc2_);
         _loc2_ = _loc2_ ^ _loc2_ >> 3 & 11795912;
         _loc2_ = (_loc2_ << 2) + _loc2_ + (_loc2_ & 255);
         _loc4_ = (_loc4_ * 5 ^ _loc2_) & 268435455;
         _loc3_ = _loc3_ + 1;
      }
      if(pflag == null && _loc4_ != _root.chk)
      {
         _p0.gotoAndStop("bad");
         body._p0.gotoAndStop("bad");
         return false;
      }
      cl.splice(2,0,dam);
      initPalette();
      playAnim = pflag;
      apply();
      return true;
   }
   return false;
}

function initPalette()
{
   var _loc7_ = this.attachMovie("palette","palinst",0);
   if(_loc7_ == null)
   {
      return undefined;
   }
   _loc7_.gotoAndStop(cl[0] + 1);
   if(cl.length >= 14)
   {
      if(cl[14] <= 9 && _loc7_.pal != null)
      {
         _loc7_.pal.gotoAndStop(1 + cl[14]);
      }
   }
   var _loc8_ = _loc7_.getBounds(_loc7_);
   var _loc6_ = new flash.display.BitmapData(_loc8_.xMax,_loc8_.yMax);
   _loc6_.draw(_loc7_);
   _loc7_.removeMovieClip();
   palette = new Array();
   var _loc4_ = 0;
   while(_loc4_ < 4)
   {
      var _loc3_ = new Array();
      var _loc5_ = _loc4_ * 15 + 7;
      while(true)
      {
         var _loc2_ = _loc6_.getPixel32(_loc3_.length * 15 + 7,_loc5_);
         if(_loc2_ == 0 || _loc2_ == -1)
         {
            break;
         }
         _loc3_.push(_loc2_ & 16777215);
      }
      palette.push(_loc3_);
      _loc4_ = _loc4_ + 1;
   }
   _loc6_.dispose();
}

function apply()
{
   applyRec(this);
   applyStatus();
}

function applyRec(mc)
{
   for(var _loc7_ in mc)
   {
      var _loc1_ = mc[_loc7_];
      if(typeof _loc1_ == "movieclip")
      {
         var _loc2_ = null;
         if(_loc1_._name.substr(0,2) == "_p")
         {
            var _loc4_ = int(_loc1_._name.substr(2,1));
            var _loc3_ = cl[_loc4_] % _loc1_._totalframes;
            _loc1_.gotoAndStop(_loc3_ + 1);
            if(_loc1_._name.substr(3,3) == "col")
            {
               _loc2_ = int(_loc1_._name.substr(6,1));
            }
         }
         else if(_loc1_._name.substr(0,4) == "_col")
         {
            _loc2_ = int(_loc1_._name.substr(4,1));
         }
         else if(_loc1_._name.substr(0,8) == "_special" && cl.length >= 15)
         {
            _loc3_ = cl[15];
            if(_loc3_ > 0)
            {
               _loc1_._visible = true;
               _loc1_.gotoAndStop(_loc3_ + 1);
            }
            else
            {
               _loc1_._visible = false;
            }
         }
         if(!playAnim)
         {
            _loc1_.stop();
         }
         if(_loc2_ != null)
         {
            var _loc5_ = palette[_loc2_];
            setColor(_loc1_,_loc5_[cl[pMax + _loc2_] % _loc5_.length]);
         }
         applyRec(_loc1_,_loc4_);
      }
   }
}

function setColor(mc, col)
{
   var _loc1_ = {r:col >> 16,g:col >> 8 & 255,b:col & 255};
   var _loc2_ = new Color(mc);
   var _loc3_ = {ra:100,ga:100,ba:100,aa:100,rb:_loc1_.r - 255,gb:_loc1_.g - 255,bb:_loc1_.b - 255,ab:0};
   _loc2_.setTransform(_loc3_);
}

function applyStatus()
{
   var _loc4_ = body._p0._p1;
   switch(status)
   {
      case "congel":
         var _loc1_ = new flash.filters.GlowFilter();
         _loc1_.blurY = _loc0_ = 14;
         _loc1_.blurX = _loc0_;
         _loc1_.strength = 1;
         _loc1_.color = 16777215;
         _loc1_.inner = true;
         var _loc3_ = new flash.filters.GlowFilter();
         _loc3_.blurY = _loc0_ = 4;
         _loc3_.blurX = _loc0_;
         _loc3_.strength = 10;
         _loc3_.color = 16777215;
         var _loc2_ = new flash.filters.GlowFilter();
         _loc2_.blurY = _loc0_ = 1.5;
         _loc2_.blurX = _loc0_;
         _loc2_.strength = 2.3;
         _loc2_.color = 6684672;
         var _loc5_ = new flash.filters.ColorMatrixFilter([0.6948261260986328,0.4100043475627899,0.05516961216926575,0,-13.64000415802002,0.2076261192560196,0.8972043395042419,0.05516961216926575,0,-13.64000415802002,0.2076261192560196,0.4100043475627899,0.5423696041107178,0,-13.64000415802002,0,0,0,1,0]);
         _loc4_.filters = [_loc1_,_loc3_,_loc2_,_loc5_];
         new Color(_loc4_).setTransform({ra:100,ga:100,ba:100,aa:100,rb:-21,gb:0,bb:135,ab:0});
         break;
      case "death":
   }
}

function checkEmbed()
{
   var _loc4_ = _root._url;
   var _loc3_ = _loc4_.split("?");
   _loc3_.shift();
   _loc3_ = _loc3_.join("?").split("&");
   for(p in _loc3_)
   {
      var _loc2_ = p.split("=")[0];
      if(_loc2_ != "v")
      {
         throw "Vars not allowed";
      }
      else
      {
         continue;
      }
   }
}

if(!_init(_root.data,int(_root.damages)))
{
   _p0.gotoAndStop("bad");
   body._p0.gotoAndStop("bad");
}

if(_root.flip != null)
{
   body._xscale = -100;
}

if(_root.alpha != null)
{
   _root._alpha = int(_root.alpha);
   _root.blendMode = "layer";
}

if(_root.onclick != null)
{
   _root.onRelease = function()
   {
      gotoURL(_root.onclick);
   };
}
