Aujourd'hui nous consacrons notre première page à Broc, ce petit bonhomme jovial d'une quarantaine d'années, toujours couvert de cambouis mais qui n'en reste pas moins un inventeur hors pair.

Dès son plus jeune âge Broc a manifesté une attirance pour les travaux manuels. Le professeur Eugène se souvient de lui comme d'un élève très passionné et très agité, les épreuves de fin d'année ont été un calvaire pour lui car il avait du mal à rester assis sur une chaise plus d'une heure, mais il a tout de même réussi ses examens. Par la suite il a obtenu une place en tant que professeur à l'université où il enseignait la mécanique.

Les choses se sont gâtées il y a une dizaine d'années, lors d'une étrange rencontre avec un individu prétendant venir de l'espace. Depuis Broc n'a qu'une obsession : pouvoir voler dans le ciel. Cela l'a poussé à mener de nombreuses expériences, plus ou moins explosives, jugées trop dangereuses par le comité de l'université. Broc s'est vu retirer son titre d'enseignant et son atelier a été déménagé au sommet du pic des îles, là où il ne gênerait pas grand monde...

Et en effet, en nous rendant sur place, nous avons vite compris pourquoi broc ne gênait personne, l'ascension est plus que périlleuse, c'est à se demander comment il a pu déménager tout son atelier. L'endroit est rempli de nombreuses machines en tout genre et au détour de l'une d'elles nous avons rencontré leur inventeur.

Broc : "Oh des visiteurs, bonjour, vous m'avez l'air bien essoufflés, vous n'avez pas pris l’ascenseur?"
Nous nous sommes alors regardés d'un air médusé : "un ascenseur ?"
Broc : "Oui c'est vrai qu'il n'est pas très bien indiqué, mais venez plutôt voir mon dernier chef d’œuvre, le Flying Brocky 6. Avec lui je devrais pouvoir survoler les steppes pour atteindre les confins de ce monde."
En traversant l'atelier, Broc nous dévoila quelques-une de ses inventions : un système de propulsion à palmes pour la barque de Jové, une machine à tresses pour castivore, un extracteur de pampleboum et j'en passe.
Arrivé à l'extrémité du hangar, Broc s'agita autour d'une console avant de se tourner vers nous et de lâcher "attention les yeux".

La machine fut catapultée dans les air et un grondement sourd se fit entendre lors de l'allumage des gaz, le jet de flamme propulsa la roquette sur plusieurs centaines de mètres avant que celle-ci n'explose en plein vol.

Broc commenta : "Comme vous le voyez le nouveau carburant, n'est pas encore très stable et mes projets ont tendance à se disperser dans tous les sens du terme. C'est pour ça que je cherche un nouvel assistant qui n'a pas froid aux yeux pour m'aider à rassembler les morceaux."

Après avoir terminé notre visite, nous avons préféré éviter l'ascenseur pour redescendre, prétextant qu'un peu d'exercice physique nous ferait du bien.